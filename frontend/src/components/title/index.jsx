import * as React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { useTranslation } from 'react-i18next';

function Title({ children }) {
  const {t} = useTranslation();
  const defaultTitleVal = t("OCWA");
  const ocwaVal = t("Output Checker Workflow App");
  return (
    <Helmet
      defaultTitle={defaultTitleVal}
      titleTemplate={defaultTitleVal + " | " + ocwaVal + " | %s"}
    >
      <title>{children}</title>
    </Helmet>
  );
}

Title.propTypes = {
  children: PropTypes.string.isRequired,
};

export default Title;
