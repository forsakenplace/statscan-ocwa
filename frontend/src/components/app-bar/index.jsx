import * as React from 'react';
import { Link, useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import Tooltip from '@atlaskit/tooltip';
import { commit, version } from '@src/services/config';
import { useTranslation } from "react-i18next";
import i18n from "i18next";

import * as styles from './styles.css';

function AppBar({ children, icon, title }) {
  const { t } = useTranslation();
  let history = useHistory();

  let langChange = () => {
    const newLang = t("flipLang");
    let newPath = window.location.pathname.replace("/"+i18n.language+"/","/"+newLang+"/");
    history.push(newPath);
  };

  const ocwaVersion = t("OCWA Version");
  return (
    <div className={styles.container}>
      <Tooltip content={ocwaVersion+` ${version}:${commit}`}>
        <Link id="app-bar-brand" to="/" className={styles.brand}>
          {icon}
          {title}
          <small>{`v${version}`}</small>
        </Link>
      </Tooltip>
      <div className={styles.actions}>
        <button onClick={langChange}>{t("langButton")}</button>
        {children}
      </div>
    </div>
  );
}

AppBar.propTypes = {
  children: PropTypes.node,
  icon: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired,
};

AppBar.defaultProps = {
  children: null,
};

export default AppBar;
