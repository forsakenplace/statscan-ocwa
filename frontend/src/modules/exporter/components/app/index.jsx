import * as React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@src/components/app-bar';
import AppBarMenu from '@src/components/app-bar/menu';
import { ButtonGroup } from '@atlaskit/button';
import Changes24Icon from '@atlaskit/icon-object/glyph/changes/24';
import Downloads from '@src/modules/download/containers/requests';
import NewRequest from '@src/modules/requests/containers/new-request';
import NotFound from '@src/components/not-found';
import RequestForm from '@src/modules/requests/containers/request-form';
import Requests from '@src/modules/requests/containers/requests-list';
import RequestTypes from '@src/modules/download/containers/request-types';
import Request from '@src/modules/requests/containers/request';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import Title from '@src/components/title';
import DownloadsLink from './downloads-link';
import * as styles from './styles.css';
import { useTranslation } from 'react-i18next';
import { useParams } from "react-router";
import i18n from "i18next";

function App({ helpURL, user, zone }) {
  let match = useRouteMatch();
  const {t} = useTranslation();

  let { lang } = useParams();
  
  if (i18n.language != lang){
    i18n.changeLanguage(lang);
  }
  
  return (
    <React.Fragment>
      <Title>{t("Exporter")}</Title>
      <AppBar icon={<Changes24Icon />} title={t("OCWA")}>
        <ButtonGroup>
          <DownloadsLink zone={zone} />
          <NewRequest />
        </ButtonGroup>
        <AppBarMenu helpURL={helpURL} user={user} />
      </AppBar>
      <div id="app-content" className={styles.container}>
        <Switch>
          <Route exact path={match.path+"/"}>
            <Requests />
          </Route>
          <Route path={match.path+"/requests/:requestId"} component={Request} />
          <Route path={match.path+"/new"}>
            <RequestForm />
          </Route>
          <Route
            path={match.path+"/downloads"}
            render={props => (
              <RequestTypes>
                <Downloads {...props} />
              </RequestTypes>
            )}
          />
          <Route>
            <NotFound />
          </Route>
        </Switch>
      </div>
    </React.Fragment>
  );
}

App.propTypes = {
  helpURL: PropTypes.string,
  user: PropTypes.shape({
    displayName: PropTypes.string.isRequired,
  }).isRequired,
  zone: PropTypes.string.isRequired,
};

App.defaultProps = {
  helpURL: null,
};

export default App;
