import * as React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@src/components/app-bar';
import AppBarMenu from '@src/components/app-bar/menu';
import Issue24Icon from '@atlaskit/icon-object/glyph/issue/24';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import NotFound from '@src/components/not-found';
import { useTranslation } from 'react-i18next';
import { useParams } from "react-router";
import i18n from "i18next";
import Dashboard from '../../containers/dashboard';
import Request from '../../containers/request';
import * as styles from './styles.css';

function App({ helpURL, user }) {
  let match = useRouteMatch();
  const {t} = useTranslation();

  let { lang } = useParams();
  
  if (i18n.language != lang){
    i18n.changeLanguage(lang);
  }
  
  return (
    <React.Fragment>
      <AppBar icon={<Issue24Icon />} title={t("OCWA Output Checker")}>
        <AppBarMenu helpURL={helpURL} user={user} />
      </AppBar>
      <div id="app-content" className={styles.container}>
        <Switch>
          <Route exact path={match.path+"/"}>
            <Dashboard />
          </Route>
          <Route path={match.path+"/requests/:requestId"} component={Request} />
          <Route>
            <NotFound />
          </Route>
        </Switch>
      </div>
    </React.Fragment>
  );
}

App.propTypes = {
  helpURL: PropTypes.string,
  user: PropTypes.shape({
    displayName: PropTypes.string.isRequired,
  }).isRequired,
};

App.defaultProps = {
  helpURL: null,
};

export default App;
