import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'react-formio';
import omit from 'lodash/omit';
import Spinner from '@atlaskit/spinner';
import translationsEN from "@src/locales/en/translations.json";
import translationsFR from "@src/locales/fr/translations.json";
import * as styles from './styles.css';
import i18n from "i18next";

function FormWrapper({ formId, isLoading, onSubmit, ...formProps }) {
  const onSubmitHandler = ({ data }) => {
    const formData = omit(data, 'submit');
    onSubmit(formData, formId);
  };

  if (isLoading) {
    return (
      <div id="request-form-wrapper-loading" className={styles.loading}>
        <Spinner size="medium" />
      </div>
    );
  }

  formProps.options={
    language: i18n.language,
    i18n:{"en":translationsEN,"fr":translationsFR}
  }

  return <Form {...formProps} onSubmit={onSubmitHandler} />;
}

FormWrapper.propTypes = {
  formId: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default FormWrapper;
