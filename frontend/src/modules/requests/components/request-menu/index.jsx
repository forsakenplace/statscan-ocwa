import * as React from 'react';
import PropTypes from 'prop-types';
import DropdownMenu, {
  DropdownItemGroup,
  DropdownItem,
} from '@atlaskit/dropdown-menu';
import inRange from 'lodash/inRange';

import { duplicateRequest } from '../../utils';
import { RequestSchema } from '../../types';
import { useTranslation } from 'react-i18next';

function RequestMenu({
  data,
  history,
  onCancel,
  onDelete,
  onSubmit,
  onWithdraw,
}) {
  const {t} = useTranslation();
  return (
    <DropdownMenu trigger={t("Actions")} triggerType="button">
      <DropdownItemGroup>
        {data.state >= 2 && data.state < 4 && (
          <React.Fragment>
            <DropdownItem onClick={() => onWithdraw(data._id)}>
              {t("Withdraw")}
            </DropdownItem>
            <DropdownItem onClick={() => onCancel(data._id)}>
              {t("Cancel")}
            </DropdownItem>
          </React.Fragment>
        )}
        {data.state < 2 && (
          <React.Fragment>
            <DropdownItem onClick={() => onSubmit(data._id)}>
              {t("Submit")}
            </DropdownItem>
            <DropdownItem
              onClick={() =>
                history.push(`/requests/${data._id}`, { isEditing: true })
              }
            >
              {t("Edit")}
            </DropdownItem>
            <DropdownItem onClick={() => onDelete(data._id)}>
              {t("Delete")}
            </DropdownItem>
          </React.Fragment>
        )}
        {inRange(data.state, 3, 7) && (
          <DropdownItem
            onClick={() => history.push('/new', duplicateRequest(data))}
          >
            {t("Duplicate")}
          </DropdownItem>
        )}
      </DropdownItemGroup>
    </DropdownMenu>
  );
}

RequestMenu.propTypes = {
  data: RequestSchema.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
  onCancel: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onWithdraw: PropTypes.func.isRequired,
};

export default RequestMenu;
