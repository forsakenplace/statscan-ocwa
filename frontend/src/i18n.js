import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import translationsEN from "./locales/en/translations.json";
import translationsFR from "./locales/fr/translations.json";

i18n.on("failedLoading", function(lng,ns,msg){
    console.log("i18n failed loading")
    console.log(lng);
    console.log(ns);
    console.log(msg);
});

i18n.use(initReactI18next).init({
    resources: {
        en: {
        translation: translationsEN
        },
        fr:{
        translation: translationsFR
        }
    },
    lng: "en",
    fallbackLng: "en",
    whitelist: ["en","fr"]
});

export default i18n;